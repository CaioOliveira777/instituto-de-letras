import styled from "styled-components";

interface Props {
    width: string,
    height: string,
    invert:boolean,
    hasImage: boolean,
}

export const Button = styled.button `
  display: flex ;
  flex-direction:row ;
  width: ${(props:Props) => (props.width)}; // Pega a width do componente pai
  height: ${(props:Props)=> (props.height)} ; // Pega a height do componente pai
  align-items:center ;
  justify-content:${(props:Props) => (props.hasImage ? 'space-between' : 'center')}; // Se tem imagem(props.hasImage =true), muda o justify content para space-between
  background: ${(props:Props)=>(props.invert ? 'transparent':'#343F94')}; // Se invert=true, fundo branco. Se false, fundo colorido
  border:${(props:Props) => (props.invert ? '1px solid #343F94': 'none')} ; // Se invert=true, tem borda. Se false, n tem;
  border-radius: 5px;
  padding-left:${(props:Props) => (props.hasImage ? '12px' : '0px')} ; // paddign se hasImage=true
  padding-right:${(props:Props) => (props.hasImage ? '12px' : '0px')} ; // paddign se hasImage=true
  h1 {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 700;
    font-size: 0.875rem;
    line-height: 136%;
    letter-spacing: 0.15px;
    color: ${(props:Props)=> (props.invert ? '#343F94' : '#FFFFFF')} ; // Cor do texto. Se invert=true, texto colorido. Se false, branco
   }
  img {
    width:22px;
    height:22px ;
   }
`;