import React from "react";
import * as S from './footerStyle'

export default function Footer(){
    return(
        <S.Footer>
            <p>Copyright @ 2022 Info Jr UFBA - Todos os direitos reservados</p>
        </S.Footer>
    )
}