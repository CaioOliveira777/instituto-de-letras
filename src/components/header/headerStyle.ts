import styled from "styled-components";

export const Header = styled.header `
width:100%;
height:80px ;
display:flex ;
flex-direction: row;
align-items:center ;
justify-content:space-between;
padding-left:16.5% ;
padding-right: 16.5% ;//18.125% ;


`;

export const Navbar = styled.nav `
display:flex ;
flex-direction:row ;
height:100% ;
align-items:center ;
a {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 500;
    font-size: 1rem;
    line-height: 150%;
    letter-spacing: 0.15px;
    color: #000000;
    text-decoration: none ;
    cursor:pointer;
}
span {
    width:24px ;
}
`;