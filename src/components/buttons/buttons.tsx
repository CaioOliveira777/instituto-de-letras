import React from "react";
import {Button} from './buttonsStyle';
import Ticket from './ticket.svg';
import Document from './document-text.svg'
/* ------ Componente de botão ------
Este botão serve para não ficarmos criando e estilizando novos botões toda hora
Legal, Caio. Mas e as props?

width - É string, no formato "20px"(exemplo); é a largura do botão

height - É string, no formato "20px"(exemplo); é a altura do botão

invert - booleano, serve para saber se o botão terá fundo branco com letras coloridas ou fundo colorido
         com letras brancas. Se TRUE, fundo branco. Se FALSE, fundo colorido

content - Contéudo, em string, que estará escrito no botão.

hasImage - booleano. Serve para saber se o botão vai ter ícone ou não. Se TRUE, terá, e o flexbox vai mudar
           Se FALSE, não terá ícone

ticketOrPaper - booleano. Caso tenha imagem, essa prop serve para colocar o ícone de Ticket ou Documento
*/
interface Props {
    width: string,
    height: string,
    invert: boolean,
    content: string,
    hasImage: boolean,
    ticketOrPaper?: boolean,
}

export default function StyledButton(props: Props){
    return (
    /*Este Button é um styled component. Para saber mais, clicar nele*/
    <Button 
        invert={props.invert}
        height={props.height}
        width={props.width}
        hasImage ={props.hasImage}
        >
        <h1>{props.content}</h1>
        {props.hasImage ? <img src={props.ticketOrPaper ? Ticket : Document} alt="img"/> : null}
    </Button>
    );
}