import React from "react";
import * as S from './headerStyle'
import StyledButton from '../buttons/buttons'

export default function Header() { 
    return (
        <S.Header>
            <S.Navbar>
                <a href="">Início</a>
                <span/> 
                <a>Sobre o evento</a>
                <span/>
                <a>Grade do evento</a>
                <span/>
                <a>Palestrantes</a>
                <span/>
                <a>Professora homenageada</a>
                <span/>
                <a>Parceiros</a>
            </S.Navbar>
            {/*Botão estilizado que criei. Para saber mais, entrar no componente*/}
            <StyledButton ticketOrPaper={true} hasImage={true} invert={false} content="Inscreva-se" height="40px" width="143px"/>
        </S.Header>
    )
}