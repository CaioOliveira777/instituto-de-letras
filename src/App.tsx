import React from 'react';
import GlobalStyles from './globalStyle';
import { MainScreen } from './mainScreenStyle';
import Header from './components/header/header'
import Section1 from './components/section1/section1';
import Footer from './components/footer/footer';



function App() {
  return (
    <MainScreen> {/*Div que vai suportar a aplicação*/}
      <GlobalStyles/> {/*estilo global para resetar o CSS e definir o tamanho da fonte responsiva*/}
      <Header/>
      <Section1/>
      {/*Aqui, vão os componentes. Por enquanto, só tem o header e footer. O resto vai entrar nesse meio aqui*/}
      <Footer/>
    </MainScreen>
  );
}

export default App;
