import styled from "styled-components";

export const Footer = styled.footer `
display:flex ;
flex-direction:row ;
height:80px ;
background:#EB5B2F ;
align-items:center;
justify-content:center ;

p {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 700;
    font-size: 1rem;
    line-height: 1.1875rem;
    color: #FFFFFF;
}

`